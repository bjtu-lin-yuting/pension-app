package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class JiGouYangLao extends AppCompatActivity {

    private ListView viewById;
    private MyHelper myHelper;
    private SimpleCursorAdapter adapter;
    private Cursor cursor;
    //SimpleCursorAdapter所需要的参数
    String from[] = new String[]{"_id", "name", "address","phone","service","price"};
    int[] to = new int[]{R.id.tv_id, R.id.tv_name, R.id.tv_address,R.id.tv_phone,R.id.tv_service,R.id.tv_price};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ji_gou_yang_lao);
        myHelper = new MyHelper(this);
        viewById = (ListView) findViewById(R.id.lv);

        //进入程序时显示数据库中的数据
        Show();
    }

    public void save(View v) {
        //获得可读写数据库对象
        SQLiteDatabase db = myHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        EditText et_name = (EditText) findViewById(R.id.et_name);
        EditText et_address = (EditText) findViewById(R.id.et_address);
        EditText et_phone = (EditText) findViewById(R.id.et_phone);
        EditText et_service = (EditText) findViewById(R.id.et_service);
        EditText et_price = (EditText) findViewById(R.id.et_price);
        values.put("name", et_name.getText().toString().trim());
        values.put("address", et_address.getText().toString());
        values.put("phone", et_phone.getText().toString());
        values.put("service", et_service.getText().toString());
        values.put("price", et_price.getText().toString());
        long q = db.insert("information", "name", values);
        Toast.makeText(this, "数据存入成功", Toast.LENGTH_SHORT).show();
        //数据库发生变化时更新listview
        cursor.requery();
        adapter.notifyDataSetChanged();
        Show();
        db.close();
    }


    /*
       当SQLite数据库中包含自增列时，会自动建立一个名为 sqlite_sequence 的表。

这个表包含两个列：name和seq。name记录自增列所在的表，seq记录当前序号（下一条记录的编号就是当前序号加1）。

如果想把某个自增列的序号归零，只需要修改 sqlite_sequence表就可以了。
        */
    public void clear(View v) {
        SQLiteDatabase db = myHelper.getWritableDatabase();
        db.delete("information", null, null);
        //使自增的_id归零
        db.delete("sqlite_sequence", null, null);
        Toast.makeText(this, "数据库清除成功", Toast.LENGTH_SHORT).show();
        cursor.requery();
        adapter.notifyDataSetChanged();
        Show();
        db.close();
    }

    //显示数据
    public void Show() {
        SQLiteDatabase db = myHelper.getWritableDatabase();
        cursor = db.query("information", null, null, null, null, null, null);
        adapter = new SimpleCursorAdapter(this, R.layout.list_item, cursor, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        viewById.setAdapter(adapter);
        db.close();
    }


}







