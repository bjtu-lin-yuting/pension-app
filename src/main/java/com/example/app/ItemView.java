package com.example.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class ItemView extends LinearLayout {
    private boolean isShowBottomLine = true;
    private boolean isShowLeftIcon = true;
    private boolean isShowRightArrow = true;
    private ImageView leftIcon;
    private TextView leftTitle;
    private TextView rightDesc;
    private ImageView rightArrow;
    private ImageView bottomLine;
    private RelativeLayout rootView;
    private AttributeSet attrs;

    public ItemView(Context context) {
        this(context, null);
    }

    public ItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        this.attrs = attrs;
    }

    public ItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //娣诲姞甯冨眬鏂囦欢
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_layout, null);
        addView(view);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ItemView);
        leftIcon = findViewById(R.id.left_icon);
        leftTitle = findViewById(R.id.left_title);
        rightDesc = findViewById(R.id.right_desc);
        rightArrow = findViewById(R.id.right_arrow);
        bottomLine = findViewById(R.id.bottom_line);
        rootView= findViewById(R.id.root_item);
        isShowBottomLine = ta.getBoolean(R.styleable.ItemView_show_bottom_line, true);
        isShowLeftIcon = ta.getBoolean(R.styleable.ItemView_show_left_icon, true);
        isShowRightArrow = ta.getBoolean(R.styleable.ItemView_show_right_arrow, true);

        leftIcon.setBackground(ta.getDrawable(R.styleable.ItemView_left_icon));
        leftIcon.setVisibility(isShowLeftIcon ? View.VISIBLE : View.INVISIBLE);

        leftTitle.setText(ta.getString(R.styleable.ItemView_left_text));
        rightDesc.setText(ta.getString(R.styleable.ItemView_right_text));

        rightArrow.setVisibility(isShowRightArrow ? View.VISIBLE : View.INVISIBLE);
        bottomLine.setVisibility(isShowBottomLine ? View.VISIBLE : View.INVISIBLE);

        rootView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClick(rightDesc.getText().toString());
            }
        });


        rightArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClick(rightDesc.getText().toString());
            }
        });

        ta.recycle();
    }


    public void setLeftIcon(int value) {
        Drawable drawable=getResources().getDrawable(value);
        leftIcon.setBackground(drawable);
    }

    public void setLeftTitle(String value) {
        leftTitle.setText(value);
    }

    public void setRightDesc(String value) {
        rightDesc.setText(value);
    }

    public void setShowRightArrow(boolean value) {
        rightArrow.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
    }


    public void setShowBottomLine(boolean value) {
        bottomLine.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
    }

    public interface itemClickListener{
        void itemClick(String text);
    }

    private itemClickListener listener;


    public void setItemClickListener(itemClickListener listener){
        this.listener=listener;
    }

}

