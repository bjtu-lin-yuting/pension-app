package com.example.app;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyHelper extends SQLiteOpenHelper {

    public MyHelper(Context context) {
        super(context, "yanglao.db", null, 1);
    }
//数据库第一次创建的时候执行

    @Override
    public void onCreate(SQLiteDatabase db) {
        //注意自增的主键名字必须是：_id
        db.execSQL("CREATE TABLE information(_id INTEGER PRIMARY KEY AUTOINCREMENT,name varchar(20)," +
                "address varchar(20),phone varchar(20),service varchar(20),price varchar(20))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}





