package com.example.app;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.app.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button imageView1=(Button) findViewById(R.id.image1);
        Button imageView2=(Button) findViewById(R.id.image2);
        Button imageView3=(Button) findViewById(R.id.image3);
        Button imageView4=(Button) findViewById(R.id.image4);
        Button imageView5=(Button) findViewById(R.id.image5);
        imageView1.setOnClickListener(l);
        imageView2.setOnClickListener(l);
        imageView3.setOnClickListener(l);
        imageView4.setOnClickListener(l);
        imageView5.setOnClickListener(l);

    }

    View.OnClickListener l=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm=getFragmentManager();//Fragment事务管理器
            FragmentTransaction ft=fm.beginTransaction();//事务
            Fragment f=null;//初始化fragment对象
            switch (view.getId()){
                case R.id.image1:
                    f=new Fragment1();
                    break;
                case R.id.image2:
                    f=new Fragment2();
                    break;
                case R.id.image3:
                    f=new Fragment3();
                    break;
                case R.id.image4:
                    f=new Fragment4();
                    break;
                case R.id.image5:
                    f=new Fragment5();
                    break;
                default:
                    break;
            }

            ft.replace(R.id.frame_layout,f);
            ft.commit();
        }
    };
}